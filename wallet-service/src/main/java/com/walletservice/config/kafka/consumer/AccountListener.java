package com.walletservice.config.kafka.consumer;

import com.google.gson.Gson;
import com.walletservice.account.Account;
import com.walletservice.account.AccountProcessor;
import com.walletservice.account.AccountRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

import static com.walletservice.util.Constant.CREATE_ACCOUNT;

@Configuration
public class AccountListener {

    Logger logger = LoggerFactory.getLogger(AccountListener.class);

    @Value("${app.start-account-value}")
    private String startAccountValue;
    @Autowired
    private Gson gson;
    @Autowired
    private AccountProcessor accountProcessor;

    @KafkaListener(topics = CREATE_ACCOUNT, groupId = "wallet-account")
    public void  createAccount(String request){
        logger.info("WALLET REQUEST STRING {} ", request);
        AccountRequest accountRequest = gson.fromJson(request, AccountRequest.class);
        logger.info("WALLET REQUEST OBJECT {} ", request);
        Account account = accountProcessor.create(accountRequest);
        logger.info("WALLET OBJECT {} ", account);
    }
}
