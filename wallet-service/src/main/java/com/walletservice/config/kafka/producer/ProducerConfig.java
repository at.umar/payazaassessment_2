package com.walletservice.config.kafka.producer;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

import static com.walletservice.util.Constant.ACCOUNT_EMAIL;

@Configuration
public class ProducerConfig {

    @Bean
    public NewTopic accountEmail(){
        return TopicBuilder
                .name(ACCOUNT_EMAIL)
                .partitions(1)
                .replicas(1)
                .build();
    }

}
