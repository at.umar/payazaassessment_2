package com.walletservice.functions;

import com.walletservice.account.AccountRequest;
import com.walletservice.account.AccountService;
import com.walletservice.model.BaseResponse;
import com.walletservice.model.TransactionRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;
import java.util.function.Supplier;

@Configuration
public class AccountManager {

    @Autowired
    AccountService accountService;

    @Bean
    public Function<AccountRequest, BaseResponse<?>> open() {
       return (accountRequest) -> accountService.createAccount(accountRequest);
    }

    @Bean
    public Function<TransactionRequest, BaseResponse<?>> fund() {
        return creditRequest -> accountService.fundAccount(creditRequest);
    }

    @Bean
    public Function<TransactionRequest, BaseResponse<?>> withdraw() {
        return debitRequest -> accountService.withdrawFromAccount(debitRequest);
    }

    @Bean
    public Supplier<BaseResponse<?>> getAccountType() {
        return () -> accountService.getAccountType();
    }

    @Bean
    public Function<Long, BaseResponse<?>> getCustomerAccount() {
        return customerId -> accountService.findAccountByCustomerId(customerId);
    }

}
