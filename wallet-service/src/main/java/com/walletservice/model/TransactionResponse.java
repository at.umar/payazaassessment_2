package com.walletservice.model;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionResponse {
    private String type;
    private BigDecimal amount;
    private String accountNumber;
}
