package com.walletservice.model;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionRequest {
    @NotNull(message = "accountId is required")
    private Long accountId;

    @NotNull(message = "customerId is required")
    private Long customerId;

    @NotNull(message = "amount is required")
    @DecimalMin(value = "100.0", message = "minimum rate is 1%")
    @DecimalMax(value = "10000000.0", message = "maximum rate is 100%")
    private BigDecimal amount;

    @NotBlank(message = "transactionPin is required")
    private String transactionPin;
}
