package com.walletservice.util;

import java.util.List;

public class Util {
    public static List<String> getAccountType(){
        return List.of(
                "FLEX",
                "DELUXE",
                "SUPA",
                "VIVA"
        );
    }
}
