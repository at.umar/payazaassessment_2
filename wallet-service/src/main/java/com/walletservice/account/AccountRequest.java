package com.walletservice.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AccountRequest {

    private String accountType;
    private String accountName;
    private String accountEmail;
    private String accountPhone;
    private Long customerId;
    private String pin;
    private String confirmPin;
    private BigDecimal openingBalance;
}
