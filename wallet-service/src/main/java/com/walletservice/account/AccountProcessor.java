package com.walletservice.account;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
@Service
@Slf4j
public class AccountProcessor {

    @Value("${app.start-account-value}")
    private String startAccountValue;
    @Autowired
    private AccountRepository accountRepository;

    public Account create(AccountRequest accountRequest){
        Account account = Account.builder()
                .accountEmail(accountRequest.getAccountEmail())
                .accountName(accountRequest.getAccountName())
                .accountPhone(accountRequest.getAccountPhone())
                .accountType(accountRequest.getAccountType())
                .accountBalance(accountRequest.getOpeningBalance())
                .accruedInterest(BigDecimal.ZERO)
                .pin(accountRequest.getPin())
                .isActive(true)
                .customerId(accountRequest.getCustomerId())
                .build();

        Account save = accountRepository.save(account);
        save.setAccountNumber(String.valueOf(account.getId() + Integer.parseInt(startAccountValue)));
       return accountRepository.save(save);
    }

}
