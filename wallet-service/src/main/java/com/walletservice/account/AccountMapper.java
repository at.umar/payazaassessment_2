package com.walletservice.account;

import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class AccountMapper implements Function<Account, AccountResponse> {
    @Override
    public AccountResponse apply(Account account) {
        return AccountResponse.builder()
                .id(account.getId())
                .accountName(account.getAccountName())
                .accountNumber(account.getAccountNumber())
                .accountType(account.getAccountType())
                .customerId(account.getCustomerId())
                .accruedInterest(account.getAccruedInterest())
                .isActive(account.getIsActive())
                .accountBalance(account.getAccountBalance())
                .build();
    }
}
