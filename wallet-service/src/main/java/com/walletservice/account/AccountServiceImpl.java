package com.walletservice.account;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.walletservice.model.BaseResponse;
import com.walletservice.model.TransactionRequest;
import com.walletservice.model.TransactionResponse;
import com.walletservice.util.Constant;
import com.walletservice.util.Util;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;
    private final AccountProcessor accountProcessor;
    private final Gson gson;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    @Value("${app.max-account-no}")
    int maxAccountNo;

    @Value("${app.min-balance}")
    long minBalance;

    @Override
    public BaseResponse<List<String>> getAccountType() {
        return new BaseResponse<>(Boolean.TRUE, "Account types", Util.getAccountType());
    }

    @Override
    public BaseResponse<Account> createAccount(AccountRequest accountRequest) {
        List<Account> accountsCount = accountRepository.findByCustomerId(accountRequest.getCustomerId());

        if (accountsCount.size() >= maxAccountNo) {
            throw new IllegalArgumentException("Maximum account limit reached");
        }

        if (!accountRequest.getPin().equals(accountRequest.getConfirmPin())) {
            throw new IllegalArgumentException("Pin not matched");
        }

        Account account = accountProcessor.create(accountRequest);
        // publish email on account created
        String subject = "Fund account";
        String message = String.format("Your account has been created with account number %s. ", account.getAccountNumber());
        JsonObject jsonObject = buildEmailRequest(account, subject, message);
        kafkaTemplate.send(Constant.ACCOUNT_EMAIL, gson.toJson(jsonObject));
        return new BaseResponse<>(Boolean.TRUE, "Customer account created", account);

    }

    @Override
    public BaseResponse<?> findAccountByCustomerId(Long customerId) {
        List<Account> byCustomerId = accountRepository.findByCustomerId(customerId);
        List<AccountResponse> collect = byCustomerId.stream().map(accountMapper).toList();
        return new BaseResponse<>(Boolean.TRUE, "Customer accounts", collect);
    }

    @Override
    public BaseResponse<?> fundAccount(TransactionRequest creditRequest) {

        Account account = accountRepository.findByIdAndCustomerId(creditRequest.getAccountId(), creditRequest.getCustomerId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid account"));

        if (Boolean.FALSE.equals(account.getIsActive())) {
            throw new IllegalArgumentException("Account is inactive");
        }

        if (creditRequest.getTransactionPin().equals(account.getPin())) {
            throw new IllegalArgumentException("Incorrect Pin");
        }

        account.setAccountBalance(account.getAccountBalance().add(creditRequest.getAmount()));
        accountRepository.save(account);

        //TODO: Log to transaction table with type credit

        //Publish transaction email notification
        String subject = "Credit alert";
        String message = String.format("Your account has been funded with %s amount", creditRequest.getAmount());
        JsonObject jsonObject = buildEmailRequest(account, subject, message);
        kafkaTemplate.send(Constant.ACCOUNT_EMAIL, gson.toJson(jsonObject));

        return BaseResponse.builder()
                .status(true)
                .message("Account funding successful")
                .data(TransactionResponse.builder()
                        .type("CREDIT")
                        .accountNumber(account.getAccountNumber())
                        .amount(creditRequest.getAmount())
                        .build())
                .build();
    }

    @Override
    public BaseResponse<?> withdrawFromAccount(TransactionRequest debitRequest) {

        Account account = accountRepository.findByIdAndCustomerId(debitRequest.getAccountId(), debitRequest.getCustomerId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid account"));
        if (Boolean.FALSE.equals(account.getIsActive())) {
            throw new IllegalArgumentException("Account is inactive");
        }

        if (debitRequest.getAmount().compareTo(account.getAccountBalance().subtract(BigDecimal.valueOf(minBalance))) >= 0) {
            throw new IllegalArgumentException("Insufficient Balance to cover this transaction");
        }

        account.setAccountBalance(account.getAccountBalance().subtract(debitRequest.getAmount()));
        accountRepository.save(account);

        //TODO: Log to transaction table with type credit

        //Publish transaction email notification
        String subject = "Debit alert";
        String message = String.format("Your account has been debited with %s amount", debitRequest.getAmount());
        JsonObject jsonObject = buildEmailRequest(account, subject, message);
        kafkaTemplate.send(Constant.ACCOUNT_EMAIL, gson.toJson(jsonObject));

        return BaseResponse.builder()
                .status(true)
                .message("Withdrawal successful")
                .build();
    }


    private JsonObject buildEmailRequest(Account account, String subject, String message) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("to", account.getAccountEmail());
        jsonObject.addProperty("subject", subject);
        jsonObject.addProperty("message", message);
        return jsonObject;
    }


}