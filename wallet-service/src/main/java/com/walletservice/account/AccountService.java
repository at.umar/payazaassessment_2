package com.walletservice.account;


import com.walletservice.model.BaseResponse;
import com.walletservice.model.TransactionRequest;

import java.math.BigDecimal;

public interface AccountService {
    BaseResponse<?> getAccountType();
    BaseResponse<?> createAccount(AccountRequest accountRequest);
    BaseResponse<?> findAccountByCustomerId(Long customerId);
    BaseResponse<?> fundAccount(TransactionRequest creditRequest);
    BaseResponse<?> withdrawFromAccount(TransactionRequest debitRequest);
}
