****Payment fintech Service****

This system serves as a payment solution for local businesses, ensuring seamless transactions. Designed as a distributed microservice, it maintains independent and loosely coupled components.

Some components of this system are built as serverless functions, capable of being deployed independently to serverless engines such as AWS Lambda, Azure Cloud Function, and others.

Apache Kafka is used as a message broker to facilitate communication between the system components, and also ensure asynchronose operation.

****Technologies****

- 1. Java/Spring boot
- 2. PostgreSQL
- 3. Apache Kafka
- 4. Spring cloud function
- 5. MailJet


****Services****

- User Servie
- Email Service
- Customer Service
- Wallet Service


****Servie and communication****

- User service: Successful user onboarding will publish email notification and customer creation events
- Email service: Listen to email notification event and send user onboarding email message
- Customer service: Listen to customer creation event and create a customer profile, then publish account opening event
- Wallet service: Listen to account opening event, then open account with a defaul account type
