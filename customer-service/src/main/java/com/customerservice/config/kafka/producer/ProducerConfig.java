package com.customerservice.config.kafka.producer;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

import static com.customerservice.util.Constant.CREATE_ACCOUNT;


@Configuration
public class ProducerConfig {

    @Bean
    public NewTopic createWallet(){
        return TopicBuilder
                .name(CREATE_ACCOUNT)
                .partitions(1)
                .replicas(1)
                .build();
    }
}
