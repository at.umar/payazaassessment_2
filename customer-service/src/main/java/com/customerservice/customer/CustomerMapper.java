package com.customerservice.customer;

import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class CustomerMapper  implements Function<Customer, CustomerResponse> {
    @Override
    public CustomerResponse apply(Customer customer) {
            return CustomerResponse.builder()
                    .id(customer.getId())
                    .userId(customer.getUserId())
                    .email(customer.getEmail())
                    .firstname(customer.getFirstName())
                    .lastname(customer.getLastName())
                    .phoneNo(customer.getPhoneNumber())
                    .build();
    }
}
