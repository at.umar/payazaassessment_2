package com.customerservice.customer;


import com.customerservice.model.BaseResponse;

public interface CustomerService {

    BaseResponse<?> createCustomer(CustomerRequest customerRequest);
    BaseResponse<?> createCustomerPin(CustomerPinRequest customerRequest);
    BaseResponse<?> changeCustomerPin(CustomerPinRequest customerRequest);
    BaseResponse<?> updateCustomer(CustomerRequest customerRequest);
    BaseResponse<?> getCustomerById(Long id);
    BaseResponse<?> deleteCustomer(Long customerId);

}
