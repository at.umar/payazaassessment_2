package com.customerservice.customer;

import com.customerservice.util.Constant;
import com.customerservice.util.Util;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static com.customerservice.util.Constant.CREATE_CUSTOMER;


@Service
public class CustomerListener {

    Logger logger = LoggerFactory.getLogger(CustomerListener.class);
    @Autowired
    KafkaTemplate<String, Object> kafkaTemplate;
    @Autowired
    Gson gson;

    @Autowired
    Util util;
    @Value("${app.min-balance}")
    long minBalance;
    @Autowired
    CustomerProcessor customerProcessor;

    @KafkaListener(topics = CREATE_CUSTOMER, groupId = "customer-id")
    public void onCreateCustomer(String request){
        logger.info("LISTENER STRING {}", request);
        CustomerRequest customerRequest = gson.fromJson(request, CustomerRequest.class);
        logger.info("LISTENER OBJECT {}", customerRequest.toString());
        Customer customer = customerProcessor.create(customerRequest);
        logger.info("CUSTOMER CREATED BY LISTENER {}", customer.toString());

        JsonObject jsonObject = util.getAccountRequest(customerRequest, customer);
        logger.info("CREATE WALLET ACCOUNT REQUEST {}", jsonObject.toString());
        kafkaTemplate.send(Constant.CREATE_ACCOUNT, gson.toJson(jsonObject));
    }
}
