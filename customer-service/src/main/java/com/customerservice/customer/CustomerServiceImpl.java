package com.customerservice.customer;


import com.customerservice.model.BaseResponse;
import com.customerservice.util.Constant;
import com.customerservice.util.Util;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;


@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerServiceImpl implements CustomerService {


    private final CustomerMapper customerMapper;
    private final CustomerRepository customerRepository;
    private final CustomerProcessor customerProcessor;
    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final Gson gson;
    private final Util util;

    private static BigDecimal calculateInterest(BigDecimal principal, double rate, long time) {
        double timeFraction = time / 365.0;
        double rateFraction = rate / 100;
        return principal.multiply(BigDecimal.valueOf(rateFraction)).multiply(BigDecimal.valueOf(timeFraction));
    }

    @Override
    @Transactional
    public BaseResponse<?> createCustomer(CustomerRequest customerRequest) {

        if (!customerRequest.getTransactionPin().equals(customerRequest.getConfirmTransactionPin())) {
            throw new IllegalArgumentException("TransactionPin and confirmTransactionPin mismatch");
        }

        Customer customer = customerProcessor.create(customerRequest);
        log.info("CREATE CUSTOMER BY ENDPOINT {}", customer.toString());

        //publish event to create user wallet account after customer registration success
        JsonObject jsonObject = util.getAccountRequest(customerRequest, customer);

        log.info("CREATE WALLET ACCOUNT REQUEST {}", jsonObject);
        kafkaTemplate.send(Constant.CREATE_ACCOUNT, gson.toJson(jsonObject));
        CustomerResponse customerResponse = customerMapper.apply(customer);

        return BaseResponse.builder()
                .status(true)
                .message("Customer Registration Successful")
                .data(customerResponse)
                .build();
    }

    @Override
    public BaseResponse<?> createCustomerPin(CustomerPinRequest customerRequest) {
        Customer customer = customerRepository.findById(customerRequest.getCustomerId())
                .orElseThrow(() -> new IllegalArgumentException("Customer with Id not found"));

        if (customerRequest.getTransactionPin() == null ||
                customerRequest.getConfirmTransactionPin() == null) {
            throw new IllegalArgumentException("Transaction pin are required");
        }

        if (!customerRequest.getTransactionPin().equals(customerRequest.getConfirmTransactionPin())) {
            throw new IllegalArgumentException("Pin not match");
        }

        customer.setTransactionPin(customerRequest.getTransactionPin());
        Customer save = customerRepository.save(customer);

        //publish event to create user wallet account after customer registration success
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("customerId", customer.getId());
        jsonObject.addProperty("accountType", customerRequest.getAccountType());
        jsonObject.addProperty("pin", customerRequest.getTransactionPin());
        jsonObject.addProperty("confirmPin", customerRequest.getConfirmTransactionPin());
        log.info("CREATE WALLET ACCOUNT REQUEST {}", jsonObject);
        kafkaTemplate.send(Constant.CREATE_ACCOUNT, gson.toJson(jsonObject));

        CustomerResponse customerResponse = customerMapper.apply(save);

        return BaseResponse.builder()
                .status(true)
                .message("Customer pin changed")
                .data(customerResponse)
                .build();
    }


    @Override
    public BaseResponse<?> changeCustomerPin(CustomerPinRequest customerRequest) {
        Customer customer = customerRepository.findById(customerRequest.getCustomerId())
                .orElseThrow(() -> new IllegalArgumentException("Customer with Id not found"));

        if (customerRequest.getTransactionPin() == null ||
                customerRequest.getConfirmTransactionPin() == null) {
            throw new IllegalArgumentException("Transaction pin are required");
        }

        if (!customerRequest.getTransactionPin().equals(customerRequest.getConfirmTransactionPin())) {
            throw new IllegalArgumentException("Pin not match");
        }

        customer.setTransactionPin(customerRequest.getTransactionPin());
        Customer save = customerRepository.save(customer);

        //publish event to create user wallet account after customer registration success
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("customerId", customer.getId());
        jsonObject.addProperty("accountType", customerRequest.getAccountType());
        jsonObject.addProperty("pin", customerRequest.getTransactionPin());
        jsonObject.addProperty("confirmPin", customerRequest.getConfirmTransactionPin());
        log.info("CREATE WALLET ACCOUNT REQUEST {}", jsonObject);
        kafkaTemplate.send(Constant.CREATE_ACCOUNT, gson.toJson(jsonObject));

        CustomerResponse customerResponse = customerMapper.apply(save);

        return BaseResponse.builder()
                .status(true)
                .message("Customer pin changed")
                .data(customerResponse)
                .build();
    }

    @Override
    public BaseResponse<?> updateCustomer(CustomerRequest customerRequest) {
        Customer customer = customerRepository.findById(customerRequest.getCustomerId())
                .orElseThrow(() -> new IllegalArgumentException("Customer with Id not found"));

        if (customerRequest.getUserId() != null) {
            customer.setUserId(customerRequest.getUserId());
        }

        if (customerRequest.getEmail() != null) {
            customer.setEmail(customerRequest.getEmail());
        }

        if (customerRequest.getFirstname() != null) {
            customer.setFirstName(customerRequest.getFirstname());
        }

        if (customerRequest.getLastname() != null) {
            customer.setLastName(customerRequest.getLastname());
        }

        if (customerRequest.getPhoneNumber() != null) {
            customer.setPhoneNumber(customerRequest.getPhoneNumber());
        }

        Customer save = customerRepository.save(customer);

        CustomerResponse customerResponse = customerMapper.apply(save);

        return BaseResponse.builder()
                .status(true)
                .message("Customer Profile updated")
                .data(customerResponse)
                .build();
    }

    @Override
    public BaseResponse<?> getCustomerById(Long id) {
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Customer with Id not found"));

        CustomerResponse customerResponse = customerMapper.apply(customer);
        return BaseResponse.builder()
                .status(true)
                .message("Customer Profile retrieved")
                .data(customerResponse)
                .build();
    }

    @Override
    public BaseResponse<?> deleteCustomer(Long customerId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer Not Found"));
        customerRepository.delete(customer);
        return BaseResponse.builder()
                .status(true)
                .message("Customer Deleted")
                .build();
    }



}
