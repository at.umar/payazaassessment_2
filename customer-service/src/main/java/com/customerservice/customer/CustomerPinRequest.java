package com.customerservice.customer;

import lombok.Data;

@Data
public class CustomerPinRequest {
    private Long customerId;
    private String accountType;
    private String transactionPin;
    private String confirmTransactionPin;
}
