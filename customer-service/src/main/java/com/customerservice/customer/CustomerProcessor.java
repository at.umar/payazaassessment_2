package com.customerservice.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerProcessor {
    @Autowired
    CustomerRepository customerRepository;
    public Customer create(CustomerRequest customerRequest){
        Customer customer = Customer.builder()
                .userId(customerRequest.getUserId())
                .firstName(customerRequest.getFirstname())
                .lastName(customerRequest.getLastname())
                .email(customerRequest.getEmail())
                .phoneNumber(customerRequest.getPhoneNumber())
                .transactionPin(customerRequest.getTransactionPin())
                .noOfAccounts(0)
                .build();
       return customerRepository.save(customer);
    }
}
