package com.customerservice.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerRequest {

    private Long userId;
    private Long customerId;
    private String firstname;
    private String lastname;
    private String email;
    private String phoneNumber;
    private String transactionPin;
    private String confirmTransactionPin;
}
