package com.customerservice.customer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerResponse {
    private Long id;
    private Long userId;
    private String firstname;
    private String lastname;
    private String email;
    private String phoneNo;
}
