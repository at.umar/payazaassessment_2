package com.customerservice.functions;

import com.customerservice.customer.CustomerRequest;
import com.customerservice.customer.CustomerService;
import com.customerservice.model.BaseResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;
@Configuration
public class CustomerManager {

    Logger logger = LoggerFactory.getLogger(CustomerManager.class);

    @Autowired
    CustomerService customerService;

    @Bean
    public Function<CustomerRequest, BaseResponse<?>> createCustomer(){
        logger.info("+++++++++++++++++ Calling create customer function ++++++++++++++++");
        return (customerRequest) -> customerService.createCustomer(customerRequest);
    }

    @Bean
    public Function<CustomerRequest, BaseResponse<?>> updateCustomer(){
        logger.info("+++++++++++++++++ Calling Update customer function ++++++++++++++++");
        return customerRequest ->  customerService.updateCustomer(customerRequest);
    }

    @Bean
    public Function<Long, BaseResponse<?>> getCustomer(){
        logger.info("+++++++++++++++++ Calling get customer by function ++++++++++++++++");
        return customerId -> customerService.getCustomerById(customerId);
    }

    @Bean
    public  Function<Long, BaseResponse<?>>  deleteCustomer(){
        logger.info("+++++++++++++++++ Calling delete customer function ++++++++++++++++");
        return customerId -> customerService.deleteCustomer(customerId);
    }

}
