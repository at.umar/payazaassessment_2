package com.customerservice.functions;

import com.customerservice.customer.CustomerPinRequest;
import com.customerservice.customer.CustomerService;
import com.customerservice.model.BaseResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;

@Configuration
public class PinManager {

    Logger logger = LoggerFactory.getLogger(PinManager.class);

    @Autowired
    CustomerService customerService;

    @Bean
    public Function<CustomerPinRequest, BaseResponse<?>> createPin(){
        logger.info("+++++++++++++++++ Calling create pin function ++++++++++++++++");
        return customerRequest ->  customerService.createCustomerPin(customerRequest);
    }

    @Bean
    public Function<CustomerPinRequest, BaseResponse<?>> changePin(){
        logger.info("+++++++++++++++++ Calling change pin function ++++++++++++++++");
        return customerRequest -> customerService.changeCustomerPin(customerRequest);
    }

}
