package com.customerservice.util;

import com.customerservice.customer.Customer;
import com.customerservice.customer.CustomerRequest;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Util {

    @Value("${app.min-balance}")
    long minBalance;

    public JsonObject getAccountRequest(CustomerRequest customerRequest, Customer customer) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("customerId", customer.getId());
        jsonObject.addProperty("accountType", "FLEX");
        jsonObject.addProperty("accountName", customer.getFirstName() + " "+ customer.getLastName());
        jsonObject.addProperty("accountEmail", customer.getEmail());
        jsonObject.addProperty("accountPhone", customer.getPhoneNumber());
        jsonObject.addProperty("openingBalance", minBalance);
        jsonObject.addProperty("openingBalance", minBalance);
        jsonObject.addProperty("pin", customerRequest.getTransactionPin());
        jsonObject.addProperty("confirmPin", customerRequest.getConfirmTransactionPin());
        return jsonObject;
    }
}
