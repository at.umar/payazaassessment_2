package com.customerservice.model;

import lombok.*;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmailRequest {
    private String to;
    private String toName;
    private String subject;
    private String message;
    private Map<String, Object> model;
}
