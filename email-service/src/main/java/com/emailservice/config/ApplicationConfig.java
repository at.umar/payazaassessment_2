package com.emailservice.config;

import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfig {

    @Bean
    public RestTemplate configRestTemplate(){
        return new RestTemplate();
    }

    @Bean
    public Gson configGson(){
        return new Gson();
    }
}
