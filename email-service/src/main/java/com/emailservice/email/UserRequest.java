package com.emailservice.email;


import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class UserRequest {
    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private String confirmPassword;


}
