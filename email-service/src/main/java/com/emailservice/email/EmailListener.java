package com.emailservice.email;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;


import static com.emailservice.util.Constant.REGISTRATION_EMAIL;

@Service
@Slf4j
public class EmailListener {


    @Autowired
    EmailService emailService;

    @Autowired
    Gson gson;
    @KafkaListener(topics = REGISTRATION_EMAIL, groupId = "emails-group")
    public void onEmailEvent(String request){
        log.info("EMAIL REQUEST STRING {}", request);
        EmailRequest emailRequest = gson.fromJson(request, EmailRequest.class);
        log.info("EMAIL REQUEST OBJECT {}", request);
        emailService.sendEmail(emailRequest);
    }
}
