package com.emailservice.email;

import com.mailjet.client.ClientOptions;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.resource.Emailv31;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    @Value("f86777ceb4eb5b80a2c786898cc43963")
    private  String mailjetPublicKey;

    @Value("8926d40dab4bd8d99b04829bc59d47a5")
    private  String mailjetSecretKey;

    @Value("chibuezenwajiobi@gmail.com")
    private  String mailjetFrom;

    @Value("PYZ_microservice")
    private  String mailjetName;

    private final RestTemplate restTemplate;

    @Override
    public void sendEmail(EmailRequest emailRequest) {
        MailjetRequest mailjetRequest;
        MailjetResponse mailjetResponse;

        ClientOptions clientOptions = ClientOptions.builder()
                .apiKey(mailjetPublicKey)
                .apiSecretKey(mailjetSecretKey)
                .build();

        try {
            MailjetClient mailjetClient = new MailjetClient(clientOptions);
            mailjetRequest = new MailjetRequest(Emailv31.resource)
                    .property(Emailv31.MESSAGES, new JSONArray()
                            .put(new JSONObject()
                                    .put(Emailv31.Message.FROM, new JSONObject()
                                            .put("Email", mailjetFrom)
                                            .put("Name", mailjetName))
                                    .put(Emailv31.Message.TO, new JSONArray()
                                            .put(new JSONObject()
                                                    .put("Email", emailRequest.getTo())
                                                    .put("Name", emailRequest.getToName())))
                                    .put(Emailv31.Message.SUBJECT, emailRequest.getSubject())
                                    .put(Emailv31.Message.HTMLPART, emailRequest.getMessage())
                            )
                    );
            mailjetResponse = mailjetClient.post(mailjetRequest);
            log.info("MailJetResponse :: status: {}, rawResponse :: {}", mailjetResponse.getStatus(), mailjetResponse.getRawResponseContent());
        } catch (MailjetException e) {
            log.error("MailjetError: " + e.getMessage());
        }
    }
}
