package com.emailservice.email;

public interface EmailService {
    void sendEmail(EmailRequest emailRequest);
}
