package com.userservice;

import com.userservice.role.Role;
import com.userservice.role.RoleRepo;
import com.userservice.user.User;
import com.userservice.user.UserRepo;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

import static com.userservice.util.Constant.ADMIN_ROLE;
import static com.userservice.util.Constant.CUSTOMER_ROLE;

@SpringBootApplication
public class UserServiceApplication {

	@Autowired
	RoleRepo roleRepo;

	@Autowired
	UserRepo userRepo;

	@Autowired
	PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(UserServiceApplication.class, args);
	}

	@PostConstruct
	public void seedRolesAndUsers() {
		// Create roles if not exist
		Role adminRole = roleRepo.findByName(ADMIN_ROLE).orElse(null);
		if (adminRole == null) {
			adminRole = new Role();
			adminRole.setName(ADMIN_ROLE);
			roleRepo.save(adminRole);
		}

		Role userRole = roleRepo.findByName(CUSTOMER_ROLE).orElse(null);
		if (userRole == null) {
			userRole = new Role();
			userRole.setName(CUSTOMER_ROLE);
			roleRepo.save(userRole);
		}

		// Create admin user if not exist
		User user = userRepo.findByUsername("admin.admin@gmail.com").orElse(null);
		if (user == null) {
			user = new User();
			user.setFirstName("admin");
			user.setLastName("admin");
			user.setUsername("admin.admin@gmail.com");
			user.setEmail("admin.admin@gmail.com");
			user.setRoles(List.of(adminRole, userRole));
			user.setEnabled(true);

			user.setPassword(passwordEncoder.encode("Pass123"));
			userRepo.save(user);
		}
	}

}
