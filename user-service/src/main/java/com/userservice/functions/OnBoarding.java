package com.userservice.functions;

import com.userservice.model.BaseResponse;
import com.userservice.user.UserRequest;
import com.userservice.user.UserResponse;
import com.userservice.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.function.Function;

public class OnBoarding implements Function<UserRequest, BaseResponse<UserResponse>> {

    Logger logger = LoggerFactory.getLogger(OnBoarding.class);

    @Autowired
    UserService userService;

    @Override
    public BaseResponse<UserResponse> apply(UserRequest userRequest) {
        logger.info("++++++++++++++++++++ Calling OnBoarding function +++++++++++++++++++++ ");
        return userService.registerUser(userRequest);
    }
}
