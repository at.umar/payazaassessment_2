package com.userservice.functions;

import com.userservice.auth.AuthService;
import com.userservice.model.BaseResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.function.Supplier;

public class Logout implements Supplier<BaseResponse<?>> {

    Logger logger = LoggerFactory.getLogger(Logout.class);
    @Autowired
    AuthService authService;

    @Override
    public BaseResponse<?> get() {
        logger.info("++++++++++++++++++++ Calling logout function +++++++++++++++++++++ ");
        return authService.logout();
    }
}
