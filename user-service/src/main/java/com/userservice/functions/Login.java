package com.userservice.functions;

import com.userservice.auth.AuthRequest;
import com.userservice.auth.AuthResponse;
import com.userservice.auth.AuthService;
import com.userservice.model.BaseResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.function.Function;

public class Login implements Function<AuthRequest, BaseResponse<AuthResponse>> {

    Logger logger = LoggerFactory.getLogger(Login.class);
    @Autowired
    AuthService authService;

    @Override
    public BaseResponse<AuthResponse> apply(AuthRequest authRequest) {
        logger.info("++++++++++++++++++++ Calling login function +++++++++++++++++++++ ");
        return authService.login(authRequest);
    }
}
