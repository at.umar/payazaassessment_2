package com.userservice.util;

public class Constant {
    public static final String REGISTRATION_EMAIL = "REGISTRATION_EMAIL";
    public static final String CREATE_CUSTOMER = "CREATE_CUSTOMER";
    public static final String ADMIN_ROLE = "ADMIN";
    public static final String CUSTOMER_ROLE = "CUSTOMER";

}
