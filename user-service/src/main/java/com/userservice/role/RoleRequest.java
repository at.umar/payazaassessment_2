package com.userservice.role;

import lombok.Data;

@Data
public class RoleRequest {
    private String name;
}
