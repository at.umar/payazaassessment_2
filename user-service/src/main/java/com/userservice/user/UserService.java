package com.userservice.user;

import com.userservice.model.BaseResponse;

public interface UserService {
    BaseResponse<UserResponse> registerUser(UserRequest userRequest);
}
