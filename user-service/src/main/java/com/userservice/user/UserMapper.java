package com.userservice.user;


import org.springframework.stereotype.Service;

import java.util.function.Function;


@Service
public class UserMapper implements Function<User, UserResponse> {
    /**
     * @param user the function argument
     * @return UserResponse
     */
    @Override
    public UserResponse apply(User user) {
        return UserResponse.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .enabled(user.getEnabled())
                .build();
    }
}
