package com.userservice.user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.userservice.exception.ApplicationException;
import com.userservice.model.BaseResponse;
import com.userservice.role.Role;
import com.userservice.role.RoleRepo;
import com.userservice.util.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.userservice.util.Constant.CUSTOMER_ROLE;

@Service
public class UserServiceImpl implements UserService{

    Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepo roleRepo;

    @Autowired
    UserMapper userMapper;

    @Autowired
    KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    UserRepo userRepo;

    @Autowired
    Gson gson;

    @Override
    public BaseResponse<UserResponse> registerUser(UserRequest userRequest) {

        List<Role> roles = new ArrayList<>();
        boolean isUserExist = userRepo.existsByEmail(userRequest.getEmail());
        if (isUserExist) {
            throw new ApplicationException("User already exist");
        }

        roleRepo.findByName(CUSTOMER_ROLE)
                .orElseThrow(() -> new ApplicationException("Role doesn't exist"));

        if (!userRequest.getPassword().equals(userRequest.getConfirmPassword())) {
            throw new ApplicationException("Passwords not match");
        }

        User newUser = User.builder()
                .firstName(userRequest.getFirstName())
                .lastName(userRequest.getLastName())
                .username(userRequest.getEmail())
                .password(passwordEncoder.encode(userRequest.getPassword()))
                .email(userRequest.getEmail())
                .phoneNumber(userRequest.getPhoneNumber())
                .roles(roles)
                .enabled(Boolean.TRUE)
                .build();

        User save = userRepo.save(newUser);
        UserResponse apply = userMapper.apply(save);

        //publish to  email service
        JsonObject emailRequest = this.buildEmailRequest(save);
        log.info("EMAIL REQUEST {}", emailRequest);
        kafkaTemplate.send(Constant.REGISTRATION_EMAIL, gson.toJson(emailRequest));

        //publish to customer service
        JsonObject customerRequest = this.buildCustomerRequest(save);
        log.info("CUSTOMER REQUEST {}", customerRequest);
        kafkaTemplate.send(Constant.CREATE_CUSTOMER, gson.toJson(customerRequest));


        return new BaseResponse<>(Boolean.TRUE, "User registered", apply);
    }

    private JsonObject buildEmailRequest(User save) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("to", save.getEmail());
        jsonObject.addProperty("subject", "User Registration");
        jsonObject.addProperty("message", "Your registration success");
        return jsonObject;
    }

    private JsonObject buildCustomerRequest(User save) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", save.getId());
        jsonObject.addProperty("firstname", save.getFirstName());
        jsonObject.addProperty("lastname", save.getLastName());
        jsonObject.addProperty("email", save.getEmail());
        jsonObject.addProperty("phoneNumber", save.getPhoneNumber());
        return jsonObject;
    }

}
