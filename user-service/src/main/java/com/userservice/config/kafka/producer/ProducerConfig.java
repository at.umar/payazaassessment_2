package com.userservice.config.kafka.producer;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

import static com.userservice.util.Constant.*;

@Configuration
public class ProducerConfig {

    @Bean
    public NewTopic registrationEmail(){
        return TopicBuilder
                .name(REGISTRATION_EMAIL)
                .partitions(1)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic createCustomer(){
        return TopicBuilder
                .name(CREATE_CUSTOMER)
                .build();
    }
}
