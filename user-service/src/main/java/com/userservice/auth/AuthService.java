package com.userservice.auth;


import com.userservice.model.BaseResponse;
import com.userservice.user.UserRequest;
import com.userservice.user.UserResponse;

public interface AuthService {
    BaseResponse<AuthResponse> login(AuthRequest request);
    BaseResponse<?> logout();

}
