package com.userservice.auth;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.userservice.config.jwt.JwtProvider;
import com.userservice.exception.ApplicationException;
import com.userservice.model.BaseResponse;
import com.userservice.role.Role;
import com.userservice.role.RoleRepo;
import com.userservice.user.*;
import com.userservice.util.Constant;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.userservice.util.Constant.CUSTOMER_ROLE;


@Service
@Slf4j
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepo userRepo;
    private final JwtProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final HttpServletRequest httpRequest;

    @Override
    public BaseResponse<AuthResponse> login(AuthRequest request) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword())
        );

        var user = userRepo.findByUsername(request.getUsername())
                .orElseThrow(() -> new ApplicationException("User not found"));

        var jwtToken = jwtTokenProvider.generateToken(user);
        var refreshToken = jwtTokenProvider.generateRefreshToken(user);
        AuthResponse authResponse = AuthResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();

        return new BaseResponse<>(Boolean.TRUE, "User logged in", authResponse);
    }

    @Override
    public BaseResponse<?> logout() {
        final String authHeader = httpRequest.getHeader("Authorization");

        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            return new BaseResponse<>(Boolean.FALSE, "Logout failed", null);
        }

        SecurityContextHolder.clearContext();
        log.info("USER LOGOUT SUCCESSFULLY");
        return new BaseResponse<>(Boolean.TRUE, "Logout successful", null);

    }
}
