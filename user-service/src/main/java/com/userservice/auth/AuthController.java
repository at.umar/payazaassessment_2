//package com.userservice.auth;
//
//
//import com.userservice.model.BaseResponse;
//import com.userservice.user.UserRequest;
//import com.userservice.user.UserResponse;
//import jakarta.validation.Valid;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//@RequestMapping("/api/v1/auth")
//@RequiredArgsConstructor
//@Slf4j
//@CrossOrigin(origins = "*")
//public class AuthController {
//
//    private final AuthServiceImpl authenticate;
//
//    @PostMapping("/register")
//    public ResponseEntity<BaseResponse<UserResponse>> createUser(@Valid @RequestBody UserRequest userRequest) {
//        BaseResponse<UserResponse> response = authenticate.registerUser(userRequest);
//        return ResponseEntity
//                .status(HttpStatus.CREATED)
//                .body(response);
//    }
//
//    @PostMapping("/login")
//    public ResponseEntity<BaseResponse<AuthResponse>> login(@Valid @RequestBody AuthRequest authRequest) {
//        BaseResponse<AuthResponse> response = authenticate.authenticate(authRequest);
//        return ResponseEntity
//                .status(HttpStatus.OK)
//                .body(response);
//    }
//
//    @GetMapping("/logout")
//    public ResponseEntity<BaseResponse<?>> login() {
//        BaseResponse<?> response = authenticate.logout();
//        return ResponseEntity
//                .status(HttpStatus.OK)
//                .body(response);
//    }
//}
